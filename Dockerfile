FROM java:8
FROM maven:alpine
MAINTAINER Hendi Santika <hendisantika@yahoo.co.id>

WORKDIR /app

COPY . /app

RUN mvn -v
RUN mvn clean install -DskipTests
EXPOSE 8080
ADD ./target/spring-boot-data-jpa-mysql-docker-composer-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","app.jar"]