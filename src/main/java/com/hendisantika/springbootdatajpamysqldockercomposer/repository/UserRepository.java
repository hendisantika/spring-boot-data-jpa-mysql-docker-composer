package com.hendisantika.springbootdatajpamysqldockercomposer.repository;

import com.hendisantika.springbootdatajpamysqldockercomposer.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-data-jpa-mysql-docker-no-composer
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-06
 * Time: 20:36
 */
public interface UserRepository extends JpaRepository<User, Long> {
}