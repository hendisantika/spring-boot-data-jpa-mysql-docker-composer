package com.hendisantika.springbootdatajpamysqldockercomposer.controller;

import com.hendisantika.springbootdatajpamysqldockercomposer.entity.User;
import com.hendisantika.springbootdatajpamysqldockercomposer.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-data-jpa-mysql-docker-no-composer
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-06
 * Time: 20:37
 */
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;


    @PostMapping("/users")
    public User create(@RequestBody User user) {
        return userRepository.save(user);
    }


    @GetMapping("/users")
    public List<User> findAll() {
        return userRepository.findAll();
    }


    @PutMapping("/users/{userid}")
    public User update(@PathVariable("userid") Long userId, @RequestBody User userObject) {
        Optional<User> userTemp = userRepository.findById(userId);
        User user = new User();
        if (userTemp.isPresent()) {
            user = userTemp.get();
            user.setName(userObject.getName());
            user.setCountry(userObject.getCountry());
        }
        return userRepository.save(user);
    }


    @DeleteMapping("/users/{user_id}")
    public List<User> delete(@PathVariable("user_id") Long userId) {
        userRepository.deleteById(userId);
        return userRepository.findAll();
    }


    @GetMapping("/users/{user_id}")
    @ResponseBody
    public Optional<User> findByUserId(@PathVariable("user_id") Long userId) {
        return userRepository.findById(userId);
    }
}
