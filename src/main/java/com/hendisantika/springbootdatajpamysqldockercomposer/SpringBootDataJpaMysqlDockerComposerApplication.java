package com.hendisantika.springbootdatajpamysqldockercomposer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDataJpaMysqlDockerComposerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDataJpaMysqlDockerComposerApplication.class, args);
    }

}

