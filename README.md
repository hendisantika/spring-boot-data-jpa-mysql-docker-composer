# spring-boot-data-jpa-mysql-docker-composer

Create Spring Boot App with Docker Composer.

__docker compose__ is a tool for defining and running multi-container **Docker** applications. 
With **Compose**, you use a **Compose** file to configure your application’s services. 
Then, using a single command, you create and start all the services from your configuration.
In addition, It allows you to define how the image should be built as well.

For this article, we are going to use and modify the same project that is created in the previous article.

In this article, i am just focusing on the docker-compose utility and related features. I am not going to describe, any spring or spring-data-jpa related features here.

First we will clone the source code of the previous article and prepare our development environment.  

This can be done with following command :
```
https://gitlab.com/hendisantika/spring-boot-data-jpa-mysql-docker-composer.git
```

**docker-compose** is a utility/tool that is used to run multi container docker applications. 
**docker-compose** utility will read the **docker-compose.yml** file for setting up the related services for the application.  
This file should contains the declaration of the services that are required to run the application.
If you need to run any service as a separate docker container, then you should declare it in the **docker-compose.yml** file.

Those services can be listed as:

* mysql service
* application service (spring boot application)

#### Setting up mysql container (service)
As you can see that, we have declared the two services here. each service will run in a separate  docker container.  lets look at each service in detailed as follows.
```
 mysql-docker-container:
 image: mysql:latest
 environment:
 - MYSQL_ROOT_PASSWORD=root123
 - MYSQL_DATABASE=spring_app_db
 - MYSQL_USER=app_user
 - MYSQL_PASSWORD=test123
 volumes:
 - /data/mysql
```

we have named the mysql service as **mysql-docker-container**. (There is no rule and it is possible to select any name for the service)

The **mysql:latest** image should be used for providing the service. In other words, this image (mysql:latest)  will be deployed in the targeted container.

we have declared four  environmental variables which will help to initialize the database, create database user and setting up root password.

volume has been defined as /data/mysql. Volumes are the preferred mechanism for persisting data generated by and used by Docker containers.

#### Before running the docker-compose
Now our **docker-compose.yml** file is ready and it is the time to up the containers with **docker-compose**.  
Before moving forward with docker-compose utility, we will look at out **Dockerfile** related to this project. 
**Dockerfile** contains the instructions of how to build the docker image with the source code.

```
FROM java:8
FROM maven:alpine
MAINTAINER Hendi Santika <hendisantika@yahoo.co.id>

WORKDIR /app

COPY . /app

RUN mvn -v
RUN mvn clean install -DskipTests
EXPOSE 8080
ADD ./target/spring-boot-data-jpa-mysql-docker-composer-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","app.jar"]
```

Just look at the above bold (highlighted) line and you will notice that docker image is built with already built **jar** file. 
The **Dockerfile** does not contains any maven or any other command to build the jar file from the project source code 
and it continues to build the docker image with already available jar file in the **target/spring-boot-data-jpa-mysql-docker-composer-0.0.1-SNAPSHOT.jar**. 
Therefore before moving with building the images or running the containers with **docker-compose**, we need to build the project artifact (*jar, war or any other related artifact file).

#### Build the project with maven
The project can be built with following maven command.
```
mvn clean install -DskipTests
```
Once the project is built, we can run the docker compose utility to build the targeted images and run the declared services in docker containers.

#### “docker-compose” to build the images and run services in docker containers
In the command line, go to the project directory where your **docker-compose.yml** file is located.  
Then run the following docker compose command to run the declared services (in **docker-compose.yml**) in docker containers.
```
docker-compose up
```

You can see that we have declare some command to build the docker image for a specific service (spring-boot-data-jpa-mysql-docker-composer) with docker-compose.  
Therefore it will build the declared image before up and running the docker services in the containers. 
You can run the following command to check whether the image is successfully built.
```
docker images
```